var devotions = [], $devotionsMarkup, allNews = [], newsMarkup,  
$eventsMarkup, allEvents = []; 

// Hide the nav links on windows load
var hideNavLinks = function() {
    $('#news').hide();
    $('#events').hide();
    $('#faqs').hide();
    $('#contactUs').hide();
    $('#single-devotion').hide();
};

// Calling the hideNavLinks on windows loading
window.onload = function() {
    hideNavLinks();
};

//Display the accordion on the FAQs section
function showAccordion() {
    $('#accordion').accordion(); 
};
showAccordion();

// Getting all the sections
var devotion, news, events, faq, contact;

devotion = $('#devotional');
news = $('#news');
events = $('#events');
faq = $('#faqs');
contact =  $('#contactUs');
singleDevotion = $('#single-devotion');
 
// Show and Hide the page
var showNavLinks = function(lnk) {
    devotion.hide();
    news.hide();
    events.hide();
    faq.hide();
    contact.hide();
    singleDevotion.hide();
    lnk.show();
};

// Function to show the devotionals page
function gotoDev() {
    $('.nav-link').on('click', showNavLinks(devotion));
};

// Function to show the news page
function gotoNews() {
    $('.nav-link').on('click', showNavLinks(news));
    $(window).scrollTop(0,0);
};

// Function to show the events page
function gotoEvts() {
    $('.nav-link').on('click', showNavLinks(events));
};

// Function to show the FAQs page
function gotoFaq() {
    $('.nav-link').on('click', showNavLinks(faq));
};

// Function to show the feedback page
function gotoContact() {
    $('.nav-link').on('click', showNavLinks(contact));
};



// Function to load the devotions on a single page
function loadSingleDevotion() {
    $('.load-more').on('click', function () {
        if (devotions) {
            const id = $(this).data('id');
            function singleDevotion(devotion) {
                return devotion.id === id;
            }
            var result = devotions.find(singleDevotion);
            var newResult = `
            <div class="container">
                <div class="row">
                    <div class="col single-devotions-markup">
                        <div class="devotion pb-4">
                            <a id="back-btn" class="py-3" href="#">
                                <span><i class="fas fa-angle-double-left"></i> Back</span>
                            </a> 
                            <h1 class="py-5 text-center display-5 text-danger">${result.devotional_date}</h1>
                            <div class="deviotional-img text-center">
                                <img src="${result.image}" alt="" class="img-fluid">
                            </div>
                            <div class="devotional-title py-2 text-center">
                                <h2>${result.title}</h2>
                                <div class="pb-1"><i class="fas fas-icon fa-calendar-alt pr-2"></i> <span>${result.daily_reading}</span></div>
                                <div class="pb-2"><i class="fas fas-icon fa-book-open pr-2"></i> <span>${result.devotional_date}</span>
                                </div>
                            </div>
                            <div class="devotional-body pt-4">
                                ${result.body}
                            </div>
                        </div>

                        <button id="comment-btn" class="btn mr-2 btn-danger">Leave a comment</button>
                        <button class="btn btn-danger share-btn">Share</button>

                        <form class="form-comment mt-4">
                            <textarea class="w-50 mb-4" name="message" id="comments" cols="30" rows="10"
                                placeholder="Please leave your comments"></textarea>
                            <button class="btn btn-warning d-block">Send</button>
                        </form>
                    </div>
                </div>
            </div>
          `;

            $('#devotional').hide();
            $('#single-devotion').children().remove();
            $('#single-devotion').append(newResult).show();
            $(window).scrollTop(0);
        }

        $('#comment-btn').on('click', function() {
            $('.form-comment').toggle();
        });

        $('#back-btn').on('click', function(e) {
            e.preventDefault();
            $('#single-devotion').hide();
            $('#devotional').show();
        });

    });

}


// Function to get the searched devotions
function searchDevotions() {
    if ($('#search-devotions').val() !== '') {
        var inputValue = $('#search-devotions').val().toLowerCase();
        var filter = devotions.filter((devotion) => devotion.title.toLowerCase().indexOf(inputValue) != -1);
        var searchFilter = filter.map(cur => {
            return `<div class="col-md-6">
                        <div class="devotion pb-4">
                            <div class="devotional-img">
                                <img src="${cur.image}" alt="" class="img">
                            </div>
                            <div class="devotional-title py-2 text-center">
                                <h2>${cur.title}</h2>
                                <div class="pb-1"><i class="fas fas-icon fa-calendar-alt pr-2"></i> <span>${cur.devotional_date}</span></div>
                                <div class="pb-2"><i class="fas fas-icon fa-book-open pr-2"></i> <span>${cur.daily_reading}</span></div>
                            </div>
                            <div class="devotional-body pt-4">
                                <p>${cur.body}</p>
                            </div>
                            <button data-id="${cur.id}" class="btn btn-secondary text-center load-more">Load More</button>
                        </div>
                    </div>`;
        });
        $devotionsMarkup.children().remove();
        $devotionsMarkup.append(searchFilter.join(''));
        $('#search-devotions').val('');
        $('.load').hide();
    }

    loadSingleDevotion();
};

// Function to get the searched news
function searchNews() {
    if ($('#search-news').val() !== '') {
        var inputValue = $('#search-news').val().toLowerCase();
        var filter = allNews.filter((oneNews) => oneNews.title.toLowerCase().indexOf(inputValue) != -1);
        var newsFilter = filter.map(cur => {
            return `
            <div class="col-md-4">
                <div class="card mt-3">
                    <img class="card-img-top img-fluid" src="${cur.image}" alt="Card image">
                    <div class="card-body">
                        <h4 class="card-title">${cur.title}</h4>
                        <p class="card-text">${cur.body}
                        </p>
                        <a href="#" class="btn active-link">Read More >></a>
                    </div>
                </div>
            </div>
            `
        });

        $newsMarkup.children().remove();
        $newsMarkup.append(newsFilter.join(''));
        $('#search-news').val('');
    }
};

//Function to get the searched events
function searchEvents() {
    if ($('#search-events').val() !== '') {
        var inputValue = $('#search-events').val().toLowerCase();
        var filter = allEvents.filter((oneEvent) => oneEvent.title.toLowerCase().indexOf(inputValue) != -1);
        var eventsFilter = filter.map(cur => {
            return `
            <div class="col-md-6">
                <div class="card mt-3">
                    <img class="card-img-top img-fluid" src="${cur.image}" alt="Card image">
                    <div class="card-body">
                        <h4 class="card-title">${cur.title}</h4>
                        <div class="pb-1"><i class="fas fas-icon fa-calendar-alt pr-2"></i> <span>${cur.event_date}</span></div>
                        <div class="pb-1"><i class="fas fas-icon fa-map-marker-alt"></i> <span>${cur.venue}</span>
                        </div>
                        <a href="#" class="btn active-link">Read More >></a>
                    </div>
                </div>
            </div>
            `
        });

        $eventsMarkup.children().remove();
        $eventsMarkup.append(eventsFilter.join(''));
        $('#search-events').val('');
    }
};

// A function to get the devotionals from the API
$(function () {
    $devotionsMarkup = $('.devotions-markup');

    var showLoader = function () {
        $('.spinner').css('display', 'block');
    };

    var removeLoader = function () {
        $('.spinner').css('display', 'none');
    }



    //Getting API request of the devotionals
    axios.get('https://api.godsheartbeat.net/api/devotionals')
        .then(data => {
            devotions = data.data.data;  // All devotions
            showLoader(); //Display loader
            var size = 2; // Set the display limit fn the devotion
            // Map through the devotions return
            var items = devotions.slice(0, size).map(cur => {
                return `<div class="col-md-6">
                     <div class="devotion pb-4">
                         <div class="devotional-img">
                             <img src="${cur.image}" alt="" class="img">
                         </div>
                         <div class="devotional-title py-2 text-center">
                             <h2>${cur.title}</h2>
                             <div class="pb-1"><i class="fas fas-icon fa-calendar-alt pr-2"></i> <span>${cur.devotional_date}</span></div>
                             <div class="pb-2"><i class="fas fas-icon fa-book-open pr-2"></i> <span>${cur.daily_reading}</span></div>
                         </div>
                         <div class="devotional-body pt-4">
                             <p>${cur.body}</p>
                         </div>
                         <button data-id="${cur.id}" class="btn btn-secondary text-center load-more">Load More</button>
                     </div>
                 </div>`;
            });

            $devotionsMarkup.append(items.join('')); //Display the devotionals
            removeLoader(); //Remove loader

            // Using the load more button to get the rest of the devotionals
            $('.load').on('click', function (e) {
                e.preventDefault();
                $devotionsMarkup.children().remove();
                showLoader();
                size += 2; // Increase the limit on the devotions
                var items = devotions.slice(0, size).map(cur => {
                    return `<div class="col-md-6">
                        <div class="devotion pb-4">
                            <div class="devotional-img">
                                <img src="${cur.image}" alt="" class="img">
                            </div>
                            <div class="devotional-title py-2 text-center">
                                <h2>${cur.title}</h2>
                                <div class="pb-1"><i class="fas fas-icon fa-calendar-alt pr-2"></i> <span>${cur.devotional_date}</span></div>
                                <div class="pb-2"><i class="fas fas-icon fa-book-open pr-2"></i> <span>${cur.daily_reading}</span></div>
                            </div>
                            <div class="devotional-body pt-4">
                                <p>${cur.body}</p>
                            </div>
                            <button data-id="${cur.id}" class="btn btn-secondary text-center load-more">Load More</button>
                        </div>
                    </div>`;
                });
                removeLoader();
                $devotionsMarkup.append(items.join(''));

                loadSingleDevotion();

            });

            // Setup the search for devotions
            $('.input-searchBtn').on('click', function (e) {
                searchDevotions();
            });

            $('.input-searchBtn').keypress(function (e) {
                if (e.keycode === 13 || e.which === 13) {
                    searchDevotions();
                }

            });

            loadSingleDevotion();
        })
        .catch(error => { console.log(error) });
});



// Using the arrow button up to scroll up the page
$('#top').on('click', function() {
    window.scroll({
        top: 0, 
        left: 0, 
        behavior: 'smooth'
    });
});


// Getting API of the News Page
$(function() {

    $newsMarkup = $('.news-row'); // News markup;
    // Show and display loader
    var showLoader = function() {
        $('.spinner-news').css('display', 'block');
    };

    var removeLoader = function() {
        $('.spinner-news').css('display', 'none');
    }

    axios.get('https://api.godsheartbeat.net/api/news')
         .then(data => {
            allNews = data.data.data;
            showLoader();
            var newsItem = allNews.map(cur => {
                return `
                <div class="col-md-4">
                    <div class="card mt-3">
                        <img class="card-img-top img-fluid" src="${cur.image}" alt="Card image">
                        <div class="card-body">
                            <h4 class="card-title">${cur.title}</h4>
                            <p class="card-text">${cur.body}
                            </p>
                            <a href="#" class="btn active-link">Read More >></a>
                        </div>
                    </div>
                </div>
                `
            });
            removeLoader();
            $newsMarkup.append(newsItem);

            //Setup for the news search
            $('#news-btn-search').on('click', function () {
                searchNews();
            });
           
         })
         .catch(error => {console.log(error)});
});


// Getting API of the Events Page

$(function() {
    $eventsMarkup = $('.events-row'); // News markup 
    allEvents = [];
    // Show and display loader
    var showLoader = function() {
        $('.spinner-news').css('display', 'block');
    };

    var removeLoader = function() {
        $('.spinner-news').css('display', 'none');
    }

    axios.get('https://api.godsheartbeat.net/api/events')
         .then(data => {
            allEvents = data.data.data;
            showLoader();
            var eventItem = allEvents.map(cur => {
                return `
                <div class="col-md-6">
                    <div class="card mt-3">
                        <img class="card-img-top img-fluid" src="${cur.image}" alt="Card image">
                        <div class="card-body">
                            <h4 class="card-title">${cur.title}</h4>
                            <div class="pb-1"><i class="fas fas-icon fa-calendar-alt pr-2"></i> <span>${cur.event_date}</span></div>
                            <div class="pb-1"><i class="fas fas-icon fa-map-marker-alt"></i> <span>${cur.venue}</span>
                            </div>
                            <a href="#" class="btn active-link">Read More >></a>
                        </div>
                    </div>
                </div>
                `
            });
            removeLoader();
            $eventsMarkup.append(eventItem);

            //Setup for the search event
            $('#event-btn-search').on('click', function() {
                searchEvents();
            });
           
         })

         .catch(error => {console.log(error)});
});
